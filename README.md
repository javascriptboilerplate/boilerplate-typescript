# boilerplate-typescript

## Description

Minimal boilerplate for [Typescript](https://www.typescriptlang.org/index.html) development using [TSLint](https://palantir.github.io/tslint/).

## Recommanded tools

1. [Visual Studio Code](https://code.visualstudio.com/)
    1. EditorConfig for VS Code
    2. TSLint (Microsoft)
    3. Code Runner (Jun Han)


Note1: Ajouter les configurations suivante dans VSC
Note2: Assurez vous de spécifier le bon chemin pour accéder a votre projet
```
  "tslint.jsEnable": true,
  "editor.codeActionsOnSave": {
    "source.fixAll.tslint": true
  },
  "javascript.format.enable": false,
  "editor.formatOnSave": true,
  "code-runner.executorMap": {
    "typescript": "D:/vsc-workspace/boilerplate-typescript/node_modules/.bin/ts-node -r tsconfig-paths/register"
  },
```

## Usage

1. Pour le development: `npm run dev`

    1. The application will be compiled and watch for code modification.
    2. If the typescript compilator detect a code change on disk, the application will be re-compile automatically.

2. Pour l'exécution

    1. Click du bouton de droit sur le fichier a exécuter (app.ts) par exemple, sélectionner **Run code**