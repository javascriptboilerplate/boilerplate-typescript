import Database from 'service/database'
import User from 'model/user'

export default class DataBaseMemodyImpl implements Database<User> {

    private users: User[] = []

    constructor() {
        const user1 = new User('Martin', 46)
        const user2 = new User('Julie', 18)
        const user3 = new User('Mario', 33)

        this.users = [user1, user2, user3]
    }

    getOne(name: string): User {
        return this.users.filter(user => user.getName() === name)[0]
    }

    getAll(): User[] {
        return this.users
    }

    addOne(user: User): void {
        this.users.push(user)
    }
}
