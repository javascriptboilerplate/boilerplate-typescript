
export default interface Database<T> {

    getOne(id: string): T

    getAll(): T[]

    addOne(item: T): void

}
