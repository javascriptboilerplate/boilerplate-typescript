
export default class User {
    name: string
    age: number

    constructor(name: string, age: number) {
        this.name = name
        this.age = age
    }

    public getName() {
        return this.name
    }

    public setName(name: string) {
        this.name = name
    }

    public getAge() {
        return this.age
    }

    public setAge(age: number) {
        this.age = age
    }
}
