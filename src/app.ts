
import Database from 'service/impl/database-memory-impl'
import User from 'model/user'

const database = new Database()

const user = database.getOne('Martin')
console.log('getOne ', user)

database.addOne(new User('Sonia', 12))

const users = database.getAll()
console.log('getAll ', users)
